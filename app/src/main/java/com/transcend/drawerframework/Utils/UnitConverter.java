package com.transcend.drawerframework.Utils;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by mike_chen on 2017/12/26.
 */

public class UnitConverter {

    private static Context mContext;

    public UnitConverter(Context context){
        mContext = context;
    }

    /**
     * Covert px to dp
     * @param px
     * @return dp
     */
    public static float convertPixelToDp(float px){
        float dp = (px*160/320) * getDensity(mContext);    //paw給的為320dpi，故px/2就等於我們要的dp，在乘上密度則完整
        return (float) (dp);  //微調數據
    }

    /**
     * Covert pt to sp
     * @param pt
     * @return sp
     */
    public static float convertPtToSp(float pt){
        float sp = (pt/100 *45) ;
        return (float) (sp);    //微調數據
    }
    /**
     * 取得螢幕密度
     * 120dpi = 0.75
     * 160dpi = 1 (default)
     * 240dpi = 1.5
     * @param context
     * @return
     */
    public static float getDensity(Context context){
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.density;
    }
}
