package com.transcend.drawerframework.Utils;

import android.view.Menu;

/**
 * Created by mike_chen on 2018/1/11.
 */

public class DrawerItemInfo{

    public String groupTitle;
    public int groupId;

    public int itemId;
    public int itemOrder;
    public String itemName;
    public int iconResId;

    public DrawerItemInfo(String title, int gid, int iid, String name, int order, int resId){
        groupTitle = title;
        groupId = gid;
        itemId = iid;
        itemOrder = order;
        itemName = name;
        iconResId = resId;
    }

    public DrawerItemInfo(int gid, int iid, String name, int resId){
        groupTitle = null;
        groupId = gid;
        itemId = iid;
        itemOrder = Menu.NONE;
        itemName = name;
        iconResId = resId;
    }

}
