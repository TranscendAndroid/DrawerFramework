package com.transcend.drawerframework;

import android.os.Build;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.transcend.drawerframework.Utils.DrawerItemInfo;
import com.transcend.drawerframework.Utils.UnitConverter;

import java.util.ArrayList;

/**
 * Created by mike_chen on 2018/1/16.
 */

public class NavigationBaseSetting{

    static String TAG = NavigationBaseSetting.class.getSimpleName();
    private DrawerLayout drawerLayout;
    private FrameLayout frameLayout;
    private NavigationView navigationView;
    private int mCurrentMenuItem = R.string.home;   //紀錄目前User位於哪一個項目
    private ImageView drawerHeader;
    private FrameLayout drawFooter;
    private static UnitConverter converter;

    private OnDrawerItemClickListener itemClickListener;
    public interface OnDrawerItemClickListener{
        void DrawerItemClick(MenuItem menuItem);
    }
    public void setDrawerItemClickListener(OnDrawerItemClickListener callback){
        this.itemClickListener = callback;
    }

    private OnFooterClickListener footerClickListener;
    public interface OnFooterClickListener {
        void FooterClick();
    }
    public void setFooterClickListener(OnFooterClickListener callback){
        this.footerClickListener = callback;
    }

    AppCompatActivity mActivity;    //欲使用Drawer的Activity
    public NavigationBaseSetting(AppCompatActivity activity){
        mActivity = activity;
    }

    public void setNavigationDrawer(int layoutResID) {  //設置Drawer
        drawerLayout = (DrawerLayout) mActivity.getLayoutInflater().inflate(R.layout.navigation_drawer, null);
        frameLayout = (FrameLayout) drawerLayout.findViewById(R.id.content_frame);
        navigationView = (NavigationView) drawerLayout.findViewById(R.id.Left_Navigation);

        converter = new UnitConverter(mActivity);
        navigationView.getLayoutParams().width = (int) converter.convertPixelToDp(590); //寬度設置
        mActivity.getLayoutInflater().inflate(layoutResID, frameLayout, true);
        mActivity.setContentView(drawerLayout);

        drawFooter = (FrameLayout) navigationView.findViewById(R.id.drawer_footer);
        mCurrentMenuItem = R.string.home;   //目前Navigation項目位置
        drawerHeader = (ImageView) navigationView.inflateHeaderView(R.layout.navigation_drawer_header).findViewById(R.id.drawer_header);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mActivity.getWindow().setStatusBarColor(mActivity.getResources().getColor(R.color.transparent));
        }
    }

    private void setUpNavigation() {
        if (navigationView==null)   //避免沒使用Drawer時被調用而導致的crash
            return;
        // Set navigation item selected listener
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if(menuItem.getItemId() == Menu.NONE)
                    return false;

                unCheckAllMenuItems(navigationView.getMenu());
                //未設置checkable時無法highlight
                menuItem.setCheckable(true);
                menuItem.setChecked(true);

                itemClickListener.DrawerItemClick(menuItem);    //Manager透過此interface得知哪個物品被點擊
                mCurrentMenuItem = menuItem.getItemId();
                return false;
            }
        });
    }

    public void unCheckAllMenuItems(Menu menu) {   //清除所有項目點擊效果
        int size = menu.size();
        for (int i = 0; i < size; i++) {
            final MenuItem item = menu.getItem(i);
            if(item.hasSubMenu()) {
                // Un check sub menu items
                unCheckAllMenuItems(item.getSubMenu());
            } else {
                item.setChecked(false);
            }
        }
    }

    public void setItemChecked(int resId){
        navigationView.getMenu().findItem(resId).setCheckable(true);
        navigationView.getMenu().findItem(resId).setChecked(true);
    }

    public void setDrawer(int headerIconRes, boolean showFooter, ArrayList<DrawerItemInfo> mDrawerList){
        if (navigationView==null)   //避免沒使用Drawer時被調用而導致的crash
            return;

        Menu menu = navigationView.getMenu();
        menu.clear();

        drawerHeader.setImageResource(headerIconRes);
        drawFooter.setVisibility(View.GONE);    //因為調整UI後會改為顯示，故要另外呼叫
        Menu submenu = null;
        String submenuTitle = null;
        for(int index = 0; index < mDrawerList.size(); index++){
            String title = mDrawerList.get(index).groupTitle;
            int gid = mDrawerList.get(index).groupId;
            int iid = mDrawerList.get(index).itemId;
            int order = mDrawerList.get(index).itemOrder;
            String name = mDrawerList.get(index).itemName;
            int iconRes = mDrawerList.get(index).iconResId;

            if(title != null){
                if(submenuTitle==null){
                    submenuTitle = title;
                    submenu = menu.addSubMenu(title);
                }
                else{
                    if(!submenuTitle.equals(title)) {
                        submenuTitle = title;
                        submenu = menu.addSubMenu(title);
                    }
                }
                submenu.add(gid, iid, order, name).setIcon(iconRes);
            }
            else
                menu.add(gid, iid, order, name).setIcon(iconRes);
        }
        //刷新介面
        navigationView.invalidate();
        setUpNavigation();
    }

    public void connectToolbarWithNavigation(Toolbar toolbar) {
        if (drawerLayout==null)   //避免沒使用Drawer時被調用而導致的crash
            return;

        //設定當使用者點擊ToolBar中的Navigation Icon時，Icon會隨著轉動。可用可不用
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle( mActivity, drawerLayout, toolbar, R.string.open_navigation, R.string.close_navigation){
            @Override
            public void onDrawerClosed(View drawerView) {
                super .onDrawerClosed(drawerView);
            }
            @Override
            public void onDrawerOpened(View drawerView) {
                super .onDrawerOpened(drawerView);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    public void enableDrawerItem(int stringResId, boolean enable){
        Menu menu = navigationView.getMenu();
        menu.findItem(stringResId).setEnabled(enable);
    }

    /**
     * SJC用以顯示使用者名稱與其信箱
     * @param userName  使用者名稱
     * @param userEmail 使用者信箱
     */
    public void setDrawerFooter(String userName, String userEmail) {

        if (navigationView==null)   //避免沒使用Drawer時被調用而導致的crash
            return;

        Menu menu = navigationView.getMenu();
        menu.add(2,Menu.NONE, Menu.NONE, "");   //留空間避免item被頁腳蓋到而無法使用

        TextView name = (TextView) drawFooter.findViewById(R.id.drawer_footer_name);
        TextView email = (TextView) drawFooter.findViewById(R.id.drawer_footer_email);
        ImageView icon = (ImageView) drawFooter.findViewById(R.id.drawer_footer_icon);
        ImageView line = (ImageView) drawFooter.findViewById(R.id.drawer_footer_line);

        drawFooter.getLayoutParams().height = (int) converter.convertPixelToDp(132);
        line.getLayoutParams().width = (int) converter.convertPixelToDp(590);
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) icon.getLayoutParams();
        lp.setMargins((int) converter.convertPixelToDp(32),0,0,0);
        icon.setLayoutParams(lp);
        lp = (ViewGroup.MarginLayoutParams) name.getLayoutParams();
        lp.setMargins((int) converter.convertPixelToDp(24),0,0,0);
        name.setLayoutParams(lp);
        lp = (ViewGroup.MarginLayoutParams) email.getLayoutParams();
        lp.setMargins((int) converter.convertPixelToDp(24),0,0,0);
        email.setLayoutParams(lp);
        name.setTextSize((int) converter.convertPtToSp(32));
        email.setTextSize((int) converter.convertPtToSp(28));
        name.setText(userName);
        email.setText(userEmail);
        drawFooter.setVisibility(View.VISIBLE);
        drawFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerClickListener.FooterClick();
            }
        });
    }

    public void setFooterIcon(int resId){
        ImageView icon = (ImageView) drawFooter.findViewById(R.id.drawer_footer_icon);
        icon.setImageResource(resId);
    }

    public MenuItem getDrawerItem(int stringResId){
        return navigationView.getMenu().findItem(stringResId);
    }

    public void setDrawerItemName(int stringResId, String name){
        navigationView.getMenu().findItem(stringResId).setTitle(name);
    }

    public void hideDrawerFooter(){
        drawFooter.setVisibility(View.GONE);
    }

    public void openDrawer(){
        drawerLayout.openDrawer(navigationView);
    }

    public void closeDrawer(){
        drawerLayout.closeDrawer(navigationView);
    }

    public DrawerLayout getDrawerLayout(){
        return drawerLayout;
    };

    public NavigationView getNavigationView(){
        return navigationView;
    };

    public int getCurrentMenuItem(){
        return mCurrentMenuItem;
    }
}